﻿using System;

namespace Autokauppa_parempi
{
    class Program
    {
        static void Main(string[] args)
        {
            Auto[] autot = new Auto[5];
            autot[0] = new Auto("Ford", "Mondeo", 3000, 300000);
            autot[1] = new Auto("Opel", "Vectra", 2000, 450000);
            autot[2] = new Auto("BMW", "M3", 14500, 57300);
            autot[3] = new Auto("Huyndai", "i30", 4500, 120000);
            autot[4] = new Auto("Ford", "Focus", 1300, 550000);
            string etsi;
            string[] automerkit = new string[] { "Ford", "Opel", "BMW", "Huyndai" };

            while (true)
            {
                Console.WriteLine("Etsi auto kirjoittamalla merkki\nKirjoita Q lopettaaksesi\nKirjoita kaikki näyttääksesi kaikki autot: ");

                etsi = Console.ReadLine();

                if (etsi == "Q") { Console.WriteLine("Lopetetaan..."); Console.WriteLine("Lopetetaan..."); Console.WriteLine("Lopetetaan..."); break; }

                if (etsi == "kaikki")
                {
                    foreach (string autoMerkki in automerkit)
                    {
                        Console.WriteLine(autoMerkki +"\n");
                    }
                }

                foreach (Auto auto in autot)
                {
                    if (auto == null)
                    { Console.WriteLine("Ei löydy! Yritä uudelleen..\n"); }

                    else if (auto.merkki == etsi)
                    {
                        Console.WriteLine(auto.ToString());
                    }
                }

            }                             
        }
    }
}


