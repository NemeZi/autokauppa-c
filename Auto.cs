﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Autokauppa_parempi
{
    class Auto
    {
        public string merkki { get; set; }
        public string malli { get; set; }
        public int hinta { get; set; }
        public int ajokilometrit { get; set; }
        public override string ToString()
        {
            return "Merkki: " + merkki + "\nMalli: " + malli + "\nHinta: " + hinta + "\nAjokilometrit: " +ajokilometrit +"\n";
        }
         
        public Auto(string amerkki, string amalli, int ahinta, int aajokilometrit)
        { merkki = amerkki; malli = amalli; hinta = ahinta; ajokilometrit = aajokilometrit; }
    }
}
